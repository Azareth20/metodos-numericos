﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metodo_de_horner
{
    class Ecuacion
    {
        public double[] Funcion;
        public double[] Potencia;
        public double[] Producto;
        public double[] DivisionSinteticaUno;
        public double[] ResiduoUno;
        public double[] ResiduoDos;
        public double iA = 0;
        public double iB = 0;
        String uno = "", dos = "", tres = "";

        public void Iniciar(int a)
        {

            Funcion = new double[a];
            Potencia = new double[a];
            Producto = new double[a];

        }

        public String mostrarFuncion()
        {
            for (int i = 0; i < Funcion.Length; i++)
            {
                if (Funcion[i] == 100)
                {
                    if (Producto[i] > 1 || Producto[i] == 1)
                    {
                        if (i == 0 || i == Funcion.Length)
                        {
                            if (Producto[i] == 1)
                            {
                                uno = "X";
                            }
                            else
                            {
                                uno = Producto[i] + "X";
                            }
                        }
                        else
                        {
                            if (Producto[i] == 1)
                            {
                                uno = "+X";
                            }
                            else
                            {
                                uno = "+" + Producto[i] + "X";
                            }
                        }
                    }
                    else if (Producto[i] < 0)
                    {
                        if (Producto[i] == -1)
                        {
                            uno = "-X";
                        }
                        else
                        {
                            uno = Producto[i] + "X";
                        }
                    }
                    else if (Producto[i] < 1 && Producto[i] > 0)
                    {
                        if (i == 0)
                        {
                            uno = Producto[i] + "X";
                        }
                        else
                        {
                            uno = "+" + Producto[i] + "X";
                        }
                    }

                    if (Potencia[i] < 2)
                    {
                        dos = uno;
                    }
                    else
                    {
                        dos = uno + "^" + Potencia[i];
                    }

                }
                else
                {
                    if (Producto[i] > 1 || Producto[i] == 1)
                    {
                        if (i == 0 || i == Funcion.Length)
                        {
                            uno = (Producto[i] * Funcion[i]) + "";
                        }
                        else
                        {
                            uno = "+" + (Producto[i] * Funcion[i]);
                        }


                    }
                    else if (Producto[i] < 0)
                    {
                        uno = (Producto[i] * Funcion[i]) + "";
                    }


                    dos = uno;
                }


                tres = tres + dos;


            }

            return tres;
        }
        // aqui empieza todo lo de Horner
        //con esta funcion lo que hace es que saca el mayor exponente de la funcion introducida
        public int Exponente()
        {
            int c = 0;
            for (int i = 0; i < Potencia.Length; i++)
            {
                if (Potencia[i] > c)
                {
                    c = (int)Potencia[i];
                }
            }
            return c;
        }

        //Con esta funcion lo que hacemos es practicamente  Calcular el residuo de cada iteracion del metodo
        public double HornerR(double pvalor)
        {
            double temp = 0;
            for (int i = 0; i < DivisionSinteticaUno.Length; i++)
            {
                if (i == 0)
                {
                    temp = DivisionSinteticaUno[i];
                }
                else
                {
                    temp = pvalor * temp;
                    temp = DivisionSinteticaUno[i] + temp;
                }
                ResiduoUno[i] = temp;
            }
            return ResiduoUno[ResiduoUno.Length - 1];
        }

        //con eso hacemos la funcion polinomica a evaluar con divisiones polinomicas
        public void funcionPolinomica()
        {
            double pvalor = Exponente();
            DivisionSinteticaUno = new double[(int)pvalor + 1];
            ResiduoUno = new double[(int)pvalor + 1];

            for (int i = 0; i < DivisionSinteticaUno.Length; i++)
            {
                if (!(i == DivisionSinteticaUno.Length - 1))
                {
                    if (exist(pvalor))
                    {
                        DivisionSinteticaUno[i] = Producto[indexExist(pvalor)];
                    }
                    else
                    {
                        DivisionSinteticaUno[i] = 0;
                    }
                }
                else
                {
                    DivisionSinteticaUno[i] = Producto[Producto.Length - 1];
                }
                pvalor--;
            }
        }
        //esto nos devuelve si el exponente se encuentra en el arreglo de potencia
        public bool exist(double pvalor)
        {
            for (int i = 0; i < Potencia.Length; i++)
            {
                if (pvalor == Potencia[i] && Funcion[i] == 100)
                {
                    return true;
                }
            }
            return false;
        }
        //esto nos da el index del valor de la potencia que se encontro
        public int indexExist(double pvalor)
        {
            int temp = 0;
            for (int i = 0; i < Potencia.Length; i++)
            {
                if (Potencia[i] == pvalor)
                {
                    temp = i;
                    break;
                }
            }
            return temp;
        }

        public double HornerAsterisco(double pvalor)
        {
            ResiduoDos = new double[ResiduoUno.Length - 1];
            double temp = 0;
            for (int i = 0; i < ResiduoUno.Length - 1; i++)
            {
                if (i == 0)
                {
                    temp = ResiduoUno[i];
                }
                else
                {
                    temp = pvalor * temp;
                    temp = ResiduoUno[i] + temp;
                }
                ResiduoDos[i] = temp;
            }
            return ResiduoDos[ResiduoDos.Length - 1];
        }

        public double redondeo(double valor, int decimales)
        {
            CRedondeo r = new CRedondeo();
            return r.redondeoExacto(valor, decimales);
        }

        public bool intervaloUsuario(double a, double b)
        {
            if (HornerR(a) * HornerR(b) <= 0)
            {
                return true;
            }
            return false;
        }

        //hacemos el algoritmo para buscar el intervalo valido para la función
        public bool intervaloAutomatico()
        {
            //todo esto se hace por si  quiere sacar el valor que mejor se aproxima al cero
            double pvalor = Producto[Producto.Length - 1];
            double valor = 0;
            int item = 0;
            if (pvalor < 0)
            {
                pvalor = pvalor * -1;
            }
            valor = pvalor;
            while (valor >= (pvalor * -1))
            {
                if (pvalor % valor == 0)
                {
                    item++;
                }
                valor--;
            }

            valor = pvalor;
            double[] intervalos = new double[item];
            for (int i = 0; i < intervalos.Length; i++)
            {
                if (pvalor % valor == 0 && valor != 0 || pvalor % valor == -0)
                {
                    intervalos[i] = valor;
                }
                else
                {
                    i--;
                }

                valor--;
            }

            double res = 0;
            double Xn = 0;
            item = 0;
            bool flag = false;
            for (int i = 0; i < intervalos.Length / 2; i++)
            {
                res = HornerR(intervalos[i]) * HornerR(intervalos[intervalos.Length - i - 1]);

                if (res <= 0)
                {
                    if (item == 0)
                    {
                        Xn = intervalos[i];
                    }
                    else
                    {
                        if (intervalos[i] < Xn)
                        {
                            Xn = intervalos[i];

                        }

                    }
                    item++;
                    flag = true;
                    //System.out.println("Es valido el intervalo " + intervalos[i] + " y el intervalo " + intervalos[intervalos.length-i-1]);
                }
                else
                {
                    //System.out.println("No valido el intervalo " + intervalos[i] + " y el intervalo " + intervalos[intervalos.length-i-1]);
                }


            }

            iA = Xn + 1;
            iB = Xn * -1;

            if (flag) return true;

            return false;
        }


    }
}
